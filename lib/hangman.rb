class Hangman
  attr_reader :guesser, :referee, :board, :guesses_remaining

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @guesses_remaining = 10
  end

  def setup
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = ["_"] * length
  end

  def take_turn
    puts "#{@guesses_remaining} guesses remaining."
    @guesses_remaining -= 1

    letter = @guesser.guess(@board)
    places = @referee.check_guess(letter)
    update_board(places, letter)
    @guesser.handle_response(places, letter)
    puts @board.join
  end

  def play
    until over?
      take_turn
    end

    if guesser_won?
      puts "The word was guessed correctly!"
    else
      puts "The word was not guesed correctly."
    end
  end

  def update_board(places, letter)
    places.each { |idx| @board[idx] = letter }
  end

  def over?
    guesser_won? || @guesses_remaining == 0
  end

  def guesser_won?
    @board.count("_") == 0
  end
end

class HumanPlayer
  attr_writer :secret_word
  attr_writer :secret_length

  def initialize
  end

  def pick_secret_word
    puts "Please decide on a secret word then enter the length of that word: "
    @secret_length = gets.chomp.to_i
  end

  def register_secret_length(length)
    @secret_length = length
  end

  def guess
    print "Enter a letter: "
    gets[0].downcase
  end

  def check_guess(letter)
    print "Where in your word does the letter '#{letter}' appear? Enter indices separated by commas: "
    input = gets.chomp
    input.split(",").map(&:to_i)
  end

  def handle_response
  end
end

class ComputerPlayer
  require 'byebug'

  attr_writer :dictionary
  attr_writer :secret_word
  attr_writer :secret_length
  attr_writer :guessed_letters
  attr_writer :possible_words

  def initialize(dictionary)
    @dictionary = dictionary.map(&:chomp)
    @guessed_letters = []
  end

  def pick_secret_word
    @secret_word = @dictionary[(@dictionary.length * rand).floor].chomp
    puts "Secret word is #{@secret_word}"
    @secret_word.length
  end

  def register_secret_length(length)
    @secret_length = length
    # filter dictionary to only words of same length
    @possible_words = @dictionary.select { |word| word.length == @secret_length }
  end

  def check_guess(letter)
    indices = []
    @secret_word.chars.each.with_index do |char, idx|
      indices << idx if char.casecmp(letter) == 0
    end
    indices
  end

  def guess(board)
    filter_dictionary(board)
    p letters = most_common_letters - @guessed_letters
    p @guessed_letters

    while board.include?(letters.last)
      @guessed_letters << letters.pop
    end

    guess = letters.pop
    @guessed_letters << guess
    guess
  end

  # reduce the possible words using some method
  def filter_dictionary(board)
    unknown_character = @guessed_letters.empty? ? "." : "[^#{@guessed_letters.join}]"
    regexp = Regexp.new("^" + board.join.gsub("_", unknown_character) + "$")

    @possible_words = @possible_words.select { |word| (word =~ regexp) == 0 }
  end

  # get most common letters
  def most_common_letters
    letters = Hash.new(0)
    @possible_words.each do |word|
      word.chomp.chars.each { |char| letters[char] += 1 }
    end
    letters.sort_by { |letter, count| count }.map { |pair| pair.first }
  end

  def handle_response(letter, places)
  end
end

if __FILE__ == $PROGRAM_NAME
  dictionary = File.readlines("lib/dictionary.txt")

  human = HumanPlayer.new
  comp = ComputerPlayer.new(dictionary)
  game = Hangman.new(guesser: comp, referee: human)

  game.setup
  game.play
end
